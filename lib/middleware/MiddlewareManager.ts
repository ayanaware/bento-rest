import { Plugin, PluginAPI } from '@ayanaware/bento';

import { Context, Next } from 'koa';
import * as compose from 'koa-compose';

import { BentoREST } from '../BentoREST';
import { Middleware, MiddlewareEntity } from './interfaces';

import { Logger } from '@ayanaware/logger-api';
const log = Logger.get();

export class MiddlewareManager implements Plugin {
	public name = '@ayanaware/bento-rest:MiddlewareManager';
	public api?: PluginAPI;

	private middlewares: Map<string, Middleware> = new Map();

	public async onLoad() {
		// add our middleware
		const bentoREST = this.api.getPlugin(BentoREST);
		bentoREST.app.use(this.middleware.bind(this));
	}

	public async onChildLoad(entity: MiddlewareEntity) {
		try {
			await this.addMiddleware(entity);
		} catch (e: any) {
			log.warn(e);
		}
	}

	public async onChildUnload(entity: MiddlewareEntity) {
		try {
			await this.removeMiddleware(entity);
		} catch (e: any) {
			log.warn(e);
		}
	}

	public async addMiddleware(middleware: Middleware) {
		if (typeof middleware.name !== 'string' || !middleware.name) throw new Error(`Middleware must have a valid name`);
		const name = middleware.name;

		if (typeof middleware.priority !== 'number') throw new Error(`"${name}".priority must be a valid number`);
		if (typeof middleware.use !== 'function') throw new Error(`"${name}".use() must be a valid middleware function`);

		this.middlewares.set(name, middleware);

		log.info(`Entity[${name}]: Registered middleware #${middleware.priority}`);
	}

	public async removeMiddleware(middleware: Middleware | string) {
		if (typeof middleware === 'object') middleware = middleware.name;

		this.middlewares.delete(middleware);
	}

	public async middleware(ctx: Context, next: Next) {
		const middlewares = Array.from(this.middlewares.values()).sort((a, b) => a.priority - b.priority).map(m => m.use);

		if (middlewares.length > 0) {
			const middleware = compose(middlewares);
			await middleware(ctx, next);
		} else return next();
	}
}
