export enum MetadataKeys {
	ROUTER = '@ayanaware/bento-rest:router',
	ROUTER_MIDDLEWARE = '@ayanaware/bento-rest:routermiddleware',
	ROUTE = '@ayanaware/bento-rest:route',
}