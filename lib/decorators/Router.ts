import { RouterOptions } from '@koa/router';
import { MetadataKeys } from './internal';

export function getRouterOptions(target: any) {
	const data: RouterOptions = Reflect.getMetadata(MetadataKeys.ROUTER, target.constructor) || {};

	return data;
}

export function Router(options: RouterOptions): ClassDecorator {
	return (target: any) => {
		if (target.prototype === undefined) target = target.constructor;
		const lastOptions: RouterOptions = Reflect.getMetadata(MetadataKeys.ROUTER, target) || {};

		// is there really a reason to merge this, when will there be more then one @Router()?
		options = { ...lastOptions, ...options };

		Reflect.defineMetadata(MetadataKeys.ROUTER, options, target);
	};
}